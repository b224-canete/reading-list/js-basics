// console.log("Hello World");

let firstName = "Lhemar";
let lastName = "Canete";

console.log(`${firstName} ${lastName} - Web Developer`);

firstName = "John";
lastName = "Doe";

console.log(firstName);
console.log(lastName);

console.log(" ");


function studentGradeEval(math, english, science, history, geography) {
	this.math = math,
	this.english = english,
	this.science = science,
	this.history = history,
	this.geography = geography
};

function evalResult(grades) {
	let sum = 0;
	let gradesArr = Object.values(grades);


	for (let i = 0; i < gradesArr.length; i++) {
		sum+= gradesArr[i];
	};

	let ave = sum/gradesArr.length;

	if(ave >= 70) {
		console.log(`Your grade is ${ave}%. You passsed the semester. Congratulations!`);
	} else {
		console.log(`Your grade is ${ave}%. You failed and have to retake the semester`);
	}
};

let student = new studentGradeEval(88,77,84,80,79);
console.log(student);
evalResult(student);

console.log(" ");

for(let i = 1; i <= 300; i++){
	if (i % 2 === 0) {
		console.log(`${i} - even`);
		continue;
	} else {
		console.log(`${i} - odd`);
	};
};

